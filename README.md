# Class 5 homework project

### Members

| Role           | Person               |
|----------------|----------------------|
|Owner       | [Boris Savov](https://is.muni.cz/auth/osoba/524847) |
|Contributor          | [Alžbeta Hajná](https://is.muni.cz/auth/osoba/493272) |
|Teacher          | [Tomáš Tomeček](https://is.muni.cz/auth/osoba/255490) |
|Teacher          | [Irina Gulina](https://is.muni.cz/auth/osoba/504090) |

# Programming Fun Facts

1. **First Computer Programmer**: Ada Lovelace is often regarded as the world's first computer programmer. She wrote the first algorithm intended to be processed by a machine, specifically Charles Babbage's Analytical Engine.

2. **Debugging Term Origin**: The term "bug" to describe a problem in code originated when a real moth got trapped in a relay of a Mark II computer in 1947. Grace Hopper, a computer scientist, removed the moth and documented it as the "first actual case" of a bug in a computer.

3. **The Hello World Tradition**: The tradition of writing a "Hello, World!" program as the first program in a new programming language was started by Brian Kernighan in the 1970s.

4. **The Most Popular Programming Language**: As of my last knowledge update in September 2021, JavaScript was one of the most popular programming languages. It's commonly used for web development.

5. **Whitespace Matters**: In Python, whitespace (indentation) is significant. The code's structure is determined by how you indent, making it unique among major programming languages.

6. **Version Control System**: Git, developed by Linus Torvalds, is one of the most popular version control systems. It's widely used for tracking changes in source code during software development.

7. **The Fibonacci Sequence in Nature**: The Fibonacci sequence, where each number is the sum of the two preceding ones, can be observed in various aspects of nature, from the arrangement of leaves on a stem to the breeding patterns of rabbits.

8. **Software Bugs**: It's estimated that there are about 15-50 bugs per 1,000 lines of code in typical software projects. This underscores the importance of rigorous testing and debugging.

9. **Moore's Law**: Named after Gordon Moore, the co-founder of Intel, Moore's Law states that the number of transistors on a microchip doubles approximately every two years, leading to exponential growth in computing power.

10. **The HAL 9000 Computer**: HAL 9000 from the movie "2001: A Space Odyssey" is one of the most famous fictional AI characters in the world of science fiction. Its calm and malevolent voice is iconic.

11. **Whitespace Programming**: There's a programming language called "Whitespace" that only uses spaces, tabs, and line breaks. All other characters are ignored. It's a fun language for code golf and obfuscation.

12. **Everyday frustration** Debugging is like being the detective in a crime movie where you are also the murderer.

13. **Y2K Bug**: The "Y2K bug," also known as the "Millennium Bug," was a programming issue where many computer systems and software represented years using only the last two digits, which caused concern that they might fail when the year 2000 arrived. Extensive testing and fixes were undertaken to prevent potential disasters.

14. **Code in Space**: In 1973, the Pioneer 10 spacecraft carried a gold-anodized aluminum plaque that contained a simple message to potential extraterrestrial life. The plaque included binary representations of the numbers 1 to 10, a depiction of the solar system, and an outline of the Pioneer 10 spacecraft. This plaque is essentially a "hello world" message from humanity to the cosmos.

Feel free to add more fun programming facts to this list or format it as you like!

